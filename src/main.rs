use std::str::FromStr;

use bpaf::{
    parsers::{NamedArg, ParseArgument},
    Parser,
};
use log::LevelFilter;
use neocities::Neocities;

pub mod util;

/// for stdout. Conditionally colourizes if supported.
macro_rules! green {
    ($item:expr) => {
        ::owo_colors::OwoColorize::if_supports_color(
            &$item,
            ::owo_colors::Stream::Stdout,
            |v: &_| {
                use ::owo_colors::{OwoColorize, Style};
                v.style(Style::new().bright_green().bold())
            },
        )
    };
}

macro_rules! yellow {
    ($item:expr) => {
        ::owo_colors::OwoColorize::if_supports_color(
            &$item,
            ::owo_colors::Stream::Stdout,
            |v: &_| {
                use ::owo_colors::{OwoColorize, Style};
                v.style(Style::new().bright_yellow().bold())
            },
        )
    };
}

macro_rules! red {
    ($item:expr) => {
        ::owo_colors::OwoColorize::if_supports_color(
            &$item,
            ::owo_colors::Stream::Stdout,
            |v: &_| {
                use ::owo_colors::{OwoColorize, Style};
                v.style(Style::new().bright_red().bold())
            },
        )
    };
}

macro_rules! blue {
    ($item:expr) => {
        ::owo_colors::OwoColorize::if_supports_color(
            &$item,
            ::owo_colors::Stream::Stdout,
            |v: &_| {
                use ::owo_colors::{OwoColorize, Style};
                v.style(Style::new().bright_blue().bold())
            },
        )
    };
}

/// Like [`NamedArg::env`] followed by [`NamedArg::argument`], except it hides the value of the
/// environment variable from the help string by using a concatenation that always uses the hidden
/// parser.
///
/// Make sure to put environment variable docs inside the help.
pub fn argument_env_hidden<T: FromStr + 'static>(
    named_arg: NamedArg,
    environment_var: &'static str,
    metavar: &'static str,
) -> impl Parser<T>
where
    ParseArgument<T>: Parser<T>,
{
    let named_arg_hidden = named_arg
        .clone()
        .env(environment_var)
        .argument::<T>(metavar)
        .hide();
    // This one just exists for the help
    let named_arg = named_arg.argument::<T>(metavar);

    bpaf::construct!([named_arg_hidden, named_arg])
}

/// Print tables where the key (lhs) is colourised, if that is allowed.  
pub mod table {
    use std::fmt::Display;

    pub fn green(lhs: impl Display, rhs: impl Display) {
        println!("{}: {}", green!(lhs), rhs)
    }

    pub fn yellow(lhs: impl Display, rhs: impl Display) {
        println!("{}: {}", yellow!(lhs), rhs)
    }

    pub fn red(lhs: impl Display, rhs: impl Display) {
        println!("{}: {}", red!(lhs), rhs)
    }

    pub fn blue(lhs: impl Display, rhs: impl Display) {
        println!("{}: {}", blue!(lhs), rhs)
    }
}

/// Pretty much just a copy of [`neocities::Auth`], which is private :(
#[derive(Clone, Hash, PartialEq, Eq)]
pub enum Auth {
    Login { sitename: String, password: String },
    Key(String),
}

impl Auth {
    pub fn into_neocities_client(self) -> Neocities {
        match self {
            Auth::Login { sitename, password } => Neocities::login(sitename, password),
            Auth::Key(key_str) => Neocities::new(key_str),
        }
    }
}

/// Construct the relevant tokio runtime.
pub fn make_runtime() -> tokio::runtime::Runtime {
    tokio::runtime::Builder::new_current_thread()
        .enable_io()
        .enable_time()
        .build()
        .expect("Could not construct async runtime!")
}

#[derive(thiserror::Error, Debug)]
pub enum IoOrReqwestError {
    #[error("General IO error - {0}")]
    Io(#[from] std::io::Error),
    #[error("Error with request - {0}")]
    Reqwest(#[from] reqwest::Error),
}

/// This is designed to at least resemble https://github.com/neocities/neocities-ruby
pub mod cli {
    use self::subcommands::{Delete, Info, Key, License, List, Push, Upload};
    use super::table::{green, red, yellow};
    use bpaf::Bpaf;
    use log::error;
    use neocities::NeocitiesError;

    pub trait Cmd {
        fn run(self) -> Result<(), anyhow::Error>;
    }

    #[derive(Bpaf)]
    #[bpaf(options, version)]
    /// Command for interacting with the NeoCities REST API, written in rust for ease of packaging
    /// and general resilience.
    pub enum FullCommand {
        License(#[bpaf(external(subcommands::license))] License),
        Info(#[bpaf(external(subcommands::info))] Info),
        Delete(#[bpaf(external(subcommands::delete))] Delete),
        Upload(#[bpaf(external(subcommands::upload))] Upload),
        Key(#[bpaf(external(subcommands::key))] Key),
        List(#[bpaf(external(subcommands::list))] List),
        Push(#[bpaf(external(subcommands::push))] Push),
    }

    impl Cmd for FullCommand {
        fn run(self) -> Result<(), anyhow::Error> {
            let result = match self {
                FullCommand::Info(c) => c.run(),
                FullCommand::License(l) => l.run(),
                FullCommand::Delete(d) => d.run(),
                FullCommand::Upload(u) => u.run(),
                FullCommand::Key(k) => k.run(),
                FullCommand::List(l) => l.run(),
                FullCommand::Push(p) => p.run(),
            };

            match result {
                v @ Ok(_) => v,
                Err(e) => {
                    error!("Error running command");
                    error!("{e}");
                    Err(e)
                }
            }
        }
    }

    #[cfg(test)]
    mod test {
        use super::full_command;

        #[test]
        fn check_cli_parser_invariants() {
            full_command().check_invariants(false);
        }
    }

    /// Display responses "as in the neocities ruby cli".
    ///
    /// This will ignore the errors from the neocities API, but leaves network/http/reqwest
    /// errors intact.
    pub fn display_response_result_and_refine(
        original_response_result: Result<String, NeocitiesError>,
    ) -> Result<(), reqwest::Error> {
        match original_response_result {
            Ok(success_message) => {
                if !success_message.is_empty() {
                    green("SUCCESS", &success_message);
                    Ok(())
                } else {
                    println!("{}", green!("SUCCESS"));
                    Ok(())
                }
            }
            Err(NeocitiesError::ApiErr(error_type, error_message))
                if error_type == "file_exists" =>
            {
                if !error_message.is_empty() {
                    yellow("EXISTS", &error_message);
                    Ok(())
                } else {
                    println!("{}", yellow!("EXISTS"));
                    Ok(())
                }
            }
            Err(NeocitiesError::ApiErr(error_type, error_message)) => {
                match (error_type.len(), error_message.len()) {
                    (0, 0) => {
                        eprintln!("{}", red!("ERROR"));
                        Ok(())
                    }
                    (0, _) => {
                        red("ERROR", &error_message);
                        Ok(())
                    }
                    (_, 0) => {
                        red("ERROR", format!("({error_type})"));
                        Ok(())
                    }
                    (_, _) => {
                        red("ERROR", format!("{error_message} ({error_type})"));
                        Ok(())
                    }
                }
            }
            Err(NeocitiesError::ReqwestErr(req_err)) => Err(req_err),
        }
    }

    pub mod subcommands {
        use std::{
            collections::HashMap,
            ffi::OsStr,
            num::NonZeroUsize,
            ops::ControlFlow,
            path::{Path, PathBuf},
            sync::Arc,
        };

        use anyhow::anyhow;
        use bpaf::{construct, Bpaf, Parser};
        use log::{error, warn};
        use neocities::Neocities;
        use sha1::Digest;
        use tokio::task::{JoinError, JoinHandle};

        use crate::{
            argument_env_hidden,
            cli::{
                display_response_result_and_refine,
                subcommands::push_cmd::{not_excluded, FileError, LocalSha1Hash},
            },
            make_runtime,
            table::{green, red, yellow},
            util::{
                recursive_directory_traverser, restricted_parallel_async,
                restricted_parallel_streaming_async, wrap_err_as_control_flow, ImpossibleErr,
                SymlinkResolvedType,
            },
            Auth, IoOrReqwestError,
        };

        use self::push_cmd::{
            display_remote::{green_lr, yellow_lr},
            relative_remote_path_as_unicode,
        };

        use super::Cmd;

        /// Authorisation parser. This is not a subcommand, but is used by subcommands that
        /// need it.
        ///
        /// This attempts to extract the API key FIRST before going for login.
        /// It only goes from environment variables, not config files.
        pub fn auth() -> impl bpaf::Parser<Auth> {
            // Not sensitive so unhidden
            let sitename = bpaf::long("sitename")
                .short('u')
                .help(
                    r#"Name of your neocities site.
If you have a simple account this should just be the name of that. If not then it should 
be the name of the specific site (as far as we can tell)."#,
                )
                .env("NEOCITIES_SITENAME")
                .argument::<String>("NEOCITIES_SITENAME");

            let password = argument_env_hidden::<String>(
                bpaf::long("password").short('p').help(
                    "[env:NEOCITIES_PASSWORD: <hidden>]\nNeocities password for your account",
                ),
                "NEOCITIES_PASSWORD",
                "NEOCITIES_PASSWORD",
            );

            let sitename_and_password = construct!(Auth::Login { sitename, password });

            let api_key = argument_env_hidden::<String>(
                bpaf::long("key")
                    .short('k')
                    .help("[env:NEOCITIES_API_KEY: <hidden>]\nAPI key for your neocities site"),
                "NEOCITIES_API_KEY",
                "NEOCITIES_API_KEY",
            )
            .map(Auth::Key);

            construct!([api_key, sitename_and_password]).group_help(
                r#"Specify the authorization method for use with neocities.

Whichever method is used first - sitename/password or api key - is what actually 
gets used. Attempts at reading the authorization from the environment variables are 
also made when arguments are unspecified, with NEOCITIES_API_KEY being of highest priority."#,
            )
        }

        #[derive(Clone, Bpaf)]
        #[bpaf(command, version)]
        /// Get information on a given site (by default, yours)
        pub struct Info {
            /// The name of the site to get.
            ///
            /// If none or empty (cus that's library behaviour), then get info
            /// for the site used for authorisation.
            #[bpaf(long("for-site"), short('f'))]
            pub non_default_site_name: Option<String>,
            #[bpaf(external(auth))]
            pub auth: Auth,
        }

        impl Info {
            /// Get the information, creating the necessary runtime
            pub fn get_info(self) -> Result<neocities::Info, neocities::NeocitiesError> {
                make_runtime().block_on(async move {
                    let req_site_name = self.non_default_site_name.unwrap_or_default();
                    let client = self.auth.into_neocities_client();
                    client.info(req_site_name).await
                })
            }
        }

        impl Cmd for Info {
            fn run(self) -> Result<(), anyhow::Error> {
                let information = self.get_info()?;
                let neocities::Info {
                    site_name,
                    hits,
                    created_at,
                    last_updated,
                    domain,
                    tags,
                } = information;

                green("Site Name", site_name);
                green("Hits", hits);
                green("Created At", created_at);
                green("Last Updated At", last_updated);

                match domain {
                    Some(domain) => green("Domain", domain),
                    None => yellow("Domain", "NONE"),
                }

                match &tags[..] {
                    [] => yellow("Tags", "NONE"),
                    tags => {
                        println!("{}", green!("Tags"));
                        for tag in tags {
                            green(" tag", tag);
                        }
                    }
                }

                Ok(())
            }
        }

        #[cfg(test)]
        mod info_test {
            /*
             * Avoiding excessive requests nya
            use bpaf::Args;

            #[test]
            /// Test on https://neocities.org/site/sadgrl
            fn sadgrl() {
                use crate::cli::{full_command, FullCommand};
                match full_command()
                    .run_inner(Args::from(&["info", "-f", "sadgrl"]))
                    .expect("should be valid")
                {
                    FullCommand::Info(info) => {
                        let data = info
                            .get_info()
                            .expect("Either there is some kind of error, or we are offline!");
                        assert!(data.tags.contains(&"blog".to_owned()));
                        assert!(data.site_name == "sadgrl")
                    }
                    _ => panic!("parsed incorrect subcommand"),
                }
            }
            */
        }

        #[derive(Bpaf, Clone)]
        #[bpaf(command, version)]
        /// Delete the passed filepaths from your neocities site.
        pub struct Delete {
            #[bpaf(external(auth))]
            pub auth: Auth,

            #[bpaf(long("ignore-network-errors"))]
            /// API errors are always ignored. However, network and HTTP errors will prevent
            /// further processing of all requests other than the concurrent ones currently
            /// happening.
            ///
            /// This will cause continuation in the face of even that.
            pub ignore_network_errors: bool,

            #[bpaf(positional, many)]
            /// Filepaths to delete on your neocities site
            pub remote_paths: Vec<String>,
        }

        impl Cmd for Delete {
            fn run(self) -> Result<(), anyhow::Error> {
                const MAX_CONCURRENT_REQUESTS: usize = 20;

                let client = Arc::new(self.auth.into_neocities_client());
                let remote_paths = self.remote_paths;
                let ignore_network_errors = self.ignore_network_errors;

                /// This executes the given load of deletion tasks concurrently (with logging)
                ///
                /// Ignoring the network errors means that errors are merely logged, else we break
                /// out early.
                ///
                /// Note that we use Arc here to ensure static lifetimes.
                ///
                /// Reqwest clients are actually entirely cloneable themselves and have an internal
                /// Arc. However, the neocities wrapper does NOT have this, and we can't so easily
                /// take references because spawning onto tokio requires a 'static future.
                ///
                /// For remote paths, we actually clone them. It makes me sad, but oh well.
                async fn concurrent_chunk_of_remote_deletion_paths(
                    remote_paths: &[String],
                    client: Arc<Neocities>,
                    ignore_network_errors: bool,
                ) -> Result<Result<(), reqwest::Error>, JoinError> {
                    // Can't borrow because tokio needs 'static futures, so clone ;-;.
                    // Makes me sad t b h nya
                    let join_handles = remote_paths
                        .iter()
                        .cloned()
                        .map(|deletion_path| {
                            let client = client.clone();
                            // construct the task
                            tokio::spawn(async move {
                                green("Deleting", &deletion_path);
                                display_response_result_and_refine(
                                    client.delete(std::slice::from_ref(&deletion_path)).await,
                                )
                            })
                        })
                        .collect::<Vec<_>>();

                    // Then go through all the tasks and wait for them, failing on error only if
                    // specified.
                    for task_handle in join_handles.into_iter() {
                        match task_handle.await? {
                            Ok(_) => continue,
                            Err(request_error) => {
                                error!("Request Error");
                                error!("{request_error}");
                                if !ignore_network_errors {
                                    return Ok(Err(request_error));
                                } else {
                                    continue;
                                }
                            }
                        }
                    }
                    Ok(Ok(()))
                }

                let runtime = make_runtime();

                // Note that the ACTUAL CLI goes path by path - so we do as well even if
                // the api lets us do bulk deletion, for the purposes of logging.
                //
                // To avoid flooding the API, we do maximum of 20 async requests at a time,
                // logging that way too.
                //
                // This for loop blocks on the futures sequentially. While it's certainly
                // possible to make a future and do a single block_on for the whole loop by using
                // .await in a loop, this seems nicer (because the ?? are inside
                // the main function rather than embedded inside the async block
                // and relying on type deduction). nya.
                for bulk_concurrent_future in
                    remote_paths
                        .chunks(MAX_CONCURRENT_REQUESTS)
                        .map(|concurrent_deletions| {
                            concurrent_chunk_of_remote_deletion_paths(
                                concurrent_deletions,
                                client.clone(),
                                ignore_network_errors,
                            )
                        })
                {
                    runtime.block_on(bulk_concurrent_future)??;
                }
                Ok(())
            }
        }

        #[derive(Bpaf, Clone)]
        #[bpaf(command, version)]
        /// Upload files (optionally in a remote directory rather than the root remote
        /// directory).
        ///
        /// This command skips directories. If you want to full on upload a whole directory, you
        /// should see the push command, which will do that for you. You also should prefer
        /// that for any bulk upload, because it will check sha1 hashes first and only upload
        /// changed files.
        ///
        /// While the ruby neocities client has a means of obtaining a sha1 hash for individual
        /// files and only uploading on change, the rust library we use does not have that
        /// capability and usually you want `push` anyhow.
        pub struct Upload {
            #[bpaf(external(auth))]
            pub auth: Auth,

            #[bpaf(short('d'), long("remote-directory"))]
            /// The remote directory to push the files into.
            ///
            /// Files, uploaded by this method, are uploaded with the name of their
            /// last path component as their name in the remote neocities site. By default, they
            /// are inserted into the root directory - however, this allows you to specify another
            /// directory they can be dumped into.
            pub remote_directory: Option<PathBuf>,

            #[bpaf(long("ignore-network-errors"))]
            /// By default neocities API errors are ignored. However, network errors cause an early
            /// exit when found.
            ///
            /// This tells the program to ignore network errors as well - this may result in weird
            /// stuff.
            pub ignore_network_errors: bool,

            #[bpaf(long("ignore-file-errors"))]
            /// By default this will exit when there are file io failures or similar.
            ///
            /// This allows ignoring those.
            pub ignore_file_errors: bool,

            #[bpaf(long("dry-run"))]
            /// Perform a dry run and don't actually upload the files.
            pub dry_run: bool,

            #[bpaf(positional, many)]
            /// List of files to upload.
            ///
            /// These files may be in arbitrary other paths - however, when uploaded,
            /// they will be given the name of their last component.
            pub local_files: Vec<PathBuf>,
        }

        impl Upload {
            /// Ensures that a remote directory is:
            /// * Absolute
            /// * Actually existing - in particular, it converts [`None`] into the root directory.
            pub fn normalize_remote_directory(remote_directory: Option<PathBuf>) -> PathBuf {
                remote_directory
                    .map(|s| {
                        // Force non-relativity on remote.
                        Path::new("/").join(s)
                    })
                    .unwrap_or_else(|| Path::new("/").to_owned())
            }

            /// Construct the valid UTF8 remote filepath to upload to from the remote
            /// directory, returning [`None`] if you should skip this file because of
            /// a number of reasons (logged inside this function).
            fn construct_remote_path(
                local_path: impl AsRef<Path>,
                remote_directory: impl AsRef<Path>,
            ) -> Option<String> {
                let local_path = local_path.as_ref();
                if !local_path.exists() {
                    error!("Path {} does not exist, skipping", local_path.display());
                    return None;
                }
                if local_path.is_dir() {
                    error!(
                        "Path {} is a directory, skipping - perhaps see the 'push' command?",
                        local_path.display()
                    );
                    return None;
                }
                let local_filename = match local_path.file_name() {
                    Some(v) => v,
                    None => {
                        error!("Local path has no discernable filename, probably ends in . or ..");
                        error!("Skipping");
                        return None;
                    }
                };
                let remote_filepath = remote_directory.as_ref().join(local_filename);
                let remote_filepath: String = match remote_filepath.to_str() {
                    Some(v) => v.to_owned(),
                    None => {
                        error!("Remote path could not be converted to UTF-8 String");
                        error!("See if your local filename or remote directory is weird");
                        return None;
                    }
                };
                Some(remote_filepath)
            }
        }

        impl Cmd for Upload {
            fn run(self) -> Result<(), anyhow::Error> {
                let Self {
                    auth,
                    remote_directory,
                    local_files,
                    ignore_network_errors,
                    dry_run,
                    ignore_file_errors,
                } = self;
                // Make the remote directory have root.
                let remote_directory = Self::normalize_remote_directory(remote_directory);

                let runtime = make_runtime();
                // Needed for concurrent access because not Clone :(
                // even though it has internal arc for shared connection pool nya >.<
                let client = Arc::new(auth.into_neocities_client());

                runtime.block_on(async move {
                    let joins = local_files
                        .into_iter()
                        .filter_map(|local_path| {
                            let remote_filepath =
                                Self::construct_remote_path(&local_path, &remote_directory);
                            remote_filepath.map(|remote_path| (remote_path, local_path))
                        })
                        .map(|(remote_filepath, local_path)| {
                            let client = client.clone();
                            async move {
                                let raw_file = match tokio::fs::File::open(&local_path).await {
                                    Ok(v) => v,
                                    Err(e) => {
                                        error!(
                                            "Could not open local path {}",
                                            local_path.display()
                                        );
                                        error!("{e}");
                                        error!("Skipping this file and upload");
                                        return Err(e.into());
                                    }
                                };

                                println!(
                                    "{} {} {} {}",
                                    green!("Uploading"),
                                    local_path.display(),
                                    green!("to"),
                                    &remote_filepath
                                );

                                display_response_result_and_refine(if dry_run {
                                    Ok("Succeeded because this is a dry run".to_owned())
                                } else {
                                    client.upload(remote_filepath, raw_file).await
                                })?;

                                Ok(())
                            }
                        })
                        .map(tokio::spawn)
                        .collect::<Vec<JoinHandle<Result<(), IoOrReqwestError>>>>();
                    // Now handle each join
                    // Forwarding join errs
                    for concurrent_future in joins {
                        match concurrent_future.await? {
                            Ok(_) => {}
                            Err(IoOrReqwestError::Io(io_err)) => {
                                if ignore_file_errors {
                                    warn!("Ignoring file error because of CLI argument")
                                } else {
                                    return Err(io_err.into());
                                }
                            }
                            Err(IoOrReqwestError::Reqwest(req_err)) => {
                                if ignore_network_errors {
                                    warn!(
                                        "Ignoring network/request error because of CLI argument."
                                    );
                                    warn!("{req_err}");
                                } else {
                                    error!("Error with request or network.");
                                    error!("{req_err}");
                                    return Err(req_err.into());
                                }
                            }
                        }
                    }

                    Ok(())
                })
            }
        }

        #[derive(Bpaf, Clone)]
        #[bpaf(command, version)]
        /// Get or generate an API key. If you already have an existing one this will get it and
        /// print it to stdout. If you don't have one (e.g. you are using username and password to
        /// login AND one has not been generated), the remote end will generate and print one.
        pub struct Key {
            #[bpaf(external(auth))]
            pub auth: Auth,
        }

        impl Cmd for Key {
            fn run(self) -> Result<(), anyhow::Error> {
                let client = self.auth.into_neocities_client();
                let key = make_runtime().block_on(client.key())?;
                println!("{key}");
                Ok(())
            }
        }

        #[derive(Bpaf, Clone)]
        #[bpaf(command, version)]
        /// List remote files and directories on the authorized neocities site.
        pub struct List {
            #[bpaf(external(auth))]
            pub auth: Auth,

            /// Whether or not to print out extra details of the files and directories.
            #[bpaf(short('d'), long("detail"))]
            pub detail: bool,

            #[bpaf(positional)]
            /// Path of the remote directory to list files in. If not specified,
            /// lists all of them.
            pub path: Option<PathBuf>,
        }

        impl List {
            /// When the path to filter on is unspecified, this should be empty, as specified
            /// in [`Neocities::list`] (which will then list all the files).
            pub fn normalise_remote_path_selector(remote_path: Option<PathBuf>) -> PathBuf {
                remote_path.unwrap_or_else(|| Path::new("").to_owned())
            }

            /// Print the given entry in the terse format (which just colourises depending on
            /// whether it's a file or directory).
            pub fn print_terse_path_info(entry: &neocities::ListEntry) {
                match entry {
                    neocities::ListEntry::File { path, .. } => println!("{}", green!(path)),
                    neocities::ListEntry::Directory { path, .. } => println!("{}", blue!(path)),
                }
            }

            /// Print the given entry in the detailed form.
            pub fn print_detailed_path_info(entry: &neocities::ListEntry) {
                match entry {
                    neocities::ListEntry::File {
                        path,
                        size,
                        updated_at,
                        sha1_hash,
                    } => {
                        println!(
                            "{}: [size:{}] [last-updated:{}] [sha1:{}]",
                            green!(path),
                            size,
                            updated_at,
                            sha1_hash
                        );
                    }
                    neocities::ListEntry::Directory { path, updated_at } => {
                        println!("{}: [last-updated:{}]", blue!(path), updated_at)
                    }
                }
            }
        }

        impl Cmd for List {
            fn run(self) -> Result<(), anyhow::Error> {
                let Self { auth, detail, path } = self;

                let path = Self::normalise_remote_path_selector(path)
                    .to_str()
                    .ok_or_else(|| {
                        anyhow!("Remote path could not be converted into a valid UTF-8 string!")
                    })?
                    .to_owned();

                let client = auth.into_neocities_client();
                let listing = make_runtime()
                    .block_on(client.list(path))
                    // Unwrap the inner errors ensuring that reqwest errors are put directly into
                    // the anyhow error
                    .map_err(|e| match e {
                        // Unlike the bulk API requests, this api request is a single one
                        // so we always want to forward all errors.
                        neocities::NeocitiesError::ApiErr(a, b) => {
                            let display_err =
                                neocities::NeocitiesError::ApiErr(a.clone(), b.clone());
                            let forwarding_err = neocities::NeocitiesError::ApiErr(a, b);

                            // This very explicitly is just a duplicated version of the error
                            // if the library type implemented clone this would be less bad
                            let _ = display_response_result_and_refine(Err(display_err));
                            anyhow::Error::from(forwarding_err)
                        }
                        neocities::NeocitiesError::ReqwestErr(e) => anyhow::Error::from(e),
                    })?;

                if detail {
                    for entry in &listing {
                        Self::print_detailed_path_info(entry);
                    }
                } else {
                    for entry in &listing {
                        Self::print_terse_path_info(entry);
                    }
                }

                Ok(())
            }
        }

        #[derive(Bpaf, Clone)]
        #[bpaf(command, version)]
        /// Push an entire directory up to your neocities website.
        pub struct Push {
            #[bpaf(external(auth))]
            pub auth: Auth,

            #[bpaf(long("dry-run"))]
            /// Perform a dry run of the push.
            pub dry_run: bool,

            #[bpaf(short('p'), long("prune"))]
            /// Remove remote files that aren't present in the local folder.
            pub prune: bool,

            #[bpaf(long("ignore-network-errors"))]
            /// By default neocities API errors are ignored. However, network errors cause an early
            /// exit when found.
            ///
            /// This tells the program to ignore network errors as well - this may result in weird
            /// stuff.
            pub ignore_network_errors: bool,

            #[bpaf(long("ignore-file-errors"))]
            /// By default this will exit when there are file io failures or similar.
            ///
            /// This allows ignoring those.
            pub ignore_file_errors: bool,

            #[bpaf(long("no-ignore-bad-hashes"))]
            /// Fail in the case that the neocities api returns an invalid sha1 hash for files,
            /// instead of using a dummy hash that forces a reupload.
            pub no_ignore_bad_hashes: bool,

            #[bpaf(short('m'), long("max-concurrency"), fallback(NonZeroUsize::try_from(2usize).expect("not zero")))]
            /// Maximum number of uploads allowed to happen at any one time. Default is 2 (previous
            /// was 20, but the neocities API returns error 104 with too high concurrency, so the
            /// default is a safe value of 2 - experiments may lead to a higher value in future). 
            pub maximum_upload_concurrency: NonZeroUsize,

            #[bpaf(short('e'), long("exclude-files"), argument("EXCLUDED_PATH_PREFIX"))]
            /// Files and directories (or more generally, path prefixes) to exclude from the upload.
            ///
            /// These are relative to the directory you are pushing, not to the current
            /// working directory. These work on directory paths as-listed (so including symlinks).
            ///
            /// Using absolute paths here will, naturally, not work (the files will simply not be
            /// excluded).
            ///
            /// These will exclude any file or directory in the pushed directory that has
            /// components matching the provided path's components at the start i.e. it acts as a
            /// simple prefix. This also means that `..` and `.` special entries will NOT work.
            /// It also will not exclude partial component matches, e.g. a/b/c will not exclude the
            /// path /a/b/comfypath from the upload, but it will exclude the paths a/b/c,
            /// a/b/c/details/ or similar.
            pub exclude_files: Vec<PathBuf>,

            #[bpaf(positional)]
            /// Directory to push up.
            pub local_directory_to_push: PathBuf,
        }

        impl Push {}

        pub mod push_cmd {
            use std::{
                borrow::Cow,
                collections::HashMap,
                ffi::{OsStr, OsString},
                fmt::{Debug, Display},
                path::{Component, Path, PathBuf},
                sync::atomic::{AtomicBool, Ordering},
            };

            use log::{error, warn};

            use crate::util::{
                hash_from_string, normalise_remote_neocities_path, HashFormatErr, SplitPath,
            };

            #[derive(Clone, Copy, Debug)]
            pub struct RemoteSha1Hash(pub [u8; 20]);

            #[derive(Clone, Copy, Debug)]
            pub struct LocalSha1Hash(pub [u8; 20]);

            /// Use special, restricted equality to prevent doing remote-on-remote hash
            /// comparisons.
            impl PartialEq<RemoteSha1Hash> for LocalSha1Hash {
                fn eq(&self, other: &RemoteSha1Hash) -> bool {
                    self.0 == other.0
                }
            }

            /// We use atomics here for multi-future and multi-threaded processing.
            pub struct ExistsLocally(pub AtomicBool);

            impl ExistsLocally {
                /// Mark that this file does in fact exist locally.
                pub fn mark_exists(&self) {
                    self.0.store(true, Ordering::Relaxed);
                }

                /// Get whether or not this exists
                pub fn exists(&self) -> bool {
                    self.0.load(Ordering::Relaxed)
                }
            }

            impl Debug for ExistsLocally {
                fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
                    if self.exists() {
                        f.write_str("exists-locally")
                    } else {
                        f.write_str("does-not-exist-locally")
                    }
                }
            }

            /// Build a remote-local existence map from the listing of directories and
            /// such returned from the neocities listing api. This could also overwrite into an
            /// existing one.
            ///
            /// This uses normalised remote neocities paths (see the function
            /// [`crate::util::normalised_remote_neocities_path`] for info).
            ///
            /// The last argument (ignore_hash_errors) should be true if you want to ignore
            /// remote api errors in sha1 hash format and use a dummy hash, which would force
            /// upload, or whether to error out.
            pub fn build_existence_map(
                fs_existence_map: &mut HashMap<String, (Option<RemoteSha1Hash>, ExistsLocally)>,
                remote_fs_list: Vec<neocities::ListEntry>,
                ignore_hash_errors: bool,
            ) -> Result<(), HashFormatErr> {
                // Arbitrary extra constant for potential new insertions after it.
                fs_existence_map.reserve(remote_fs_list.len() + 50);

                // Note that remote directories are listed by neocities all up the tree as well, so
                // yeah, the hashmap will always have them.
                remote_fs_list.into_iter().try_for_each(
                    |remote_item| -> Result<(), HashFormatErr> {
                        match remote_item {
                            neocities::ListEntry::File {
                                path,
                                size: _,
                                updated_at: _,
                                sha1_hash,
                            } => {
                                let validated_hash: RemoteSha1Hash = RemoteSha1Hash(
                                    hash_from_string(sha1_hash.as_str()).or_else(|err| {
                                        warn!(
                                            "Remote file {path} had a bad hash string \"{sha1_hash}\""
                                        );
                                        warn!("{err}");
                                        if !ignore_hash_errors {
                                            error!("Aborting");
                                            Err(err)
                                        } else {
                                            warn!("Using dummy hash to force reupload if present");
                                            Ok([0; 20])
                                        }
                                    })?,
                                );
                                fs_existence_map.insert(
                                    normalise_remote_neocities_path(Cow::Owned(path)).into_owned(),
                                    (Some(validated_hash), ExistsLocally(false.into())),
                                );
                                Ok(())
                            }
                            neocities::ListEntry::Directory { path, updated_at: _ } => {
                                let path = normalise_remote_neocities_path(Cow::Owned(path)).into_owned();
                                fs_existence_map.insert(path, (None, ExistsLocally(false.into())));
                                Ok(())
                            },
                        }
                    },
                )
            }

            /// Assert that the given path (in the form of OsString chunks) relative to the
            /// uploaded directory is not excluded by the list of excluded paths.
            ///
            /// For details on functionality, see the CLI option.
            pub fn not_excluded(
                relative_remote_path: &SplitPath,
                excluded_paths: &[PathBuf],
            ) -> bool {
                !excluded_paths
                    .iter()
                    .map(|excluded_path| -> bool {
                        // If the pattern component is longer than  the actual path, then of course it
                        // will not match.
                        //
                        // Because components all have to be bare OsStr and no root components or
                        // similar, this simple comparison is fine, as anything that alters the count
                        // would result in not excluding things anyway.
                        //
                        // Without this check, if the checked path was shorter than the pattern, then
                        // it would still match the pattern because zip would run out of the checked
                        // component iterator elements.
                        if excluded_path.components().count() > relative_remote_path.len() {
                            return false;
                        };
                        excluded_path
                            .components()
                            .zip(relative_remote_path.iter())
                            .map(|(pattern_component, validated_component)| {
                                match (pattern_component, validated_component) {
                                    (
                                        Component::Prefix(_)
                                        | Component::CurDir
                                        | Component::ParentDir
                                        | Component::RootDir,
                                        _matched_component,
                                    ) => false,
                                    (Component::Normal(component), matched_component) => {
                                        component == matched_component.as_os_str()
                                    }
                                }
                            })
                            .all(|current_component_matches| current_component_matches)
                    })
                    .any(|current_excludes| current_excludes)
            }

            #[cfg(test)]
            pub mod exclusion_test {
                use std::path::Path;

                use smallvec::smallvec;

                use crate::util::SplitPath;

                use super::not_excluded;

                #[test]
                pub fn empty_has_no_exclusion() {
                    let exclusion_pat = vec![];
                    let splitpath: SplitPath = smallvec!["a".into(), "r".into(), "baaaa".into()];
                    assert!(not_excluded(&splitpath, exclusion_pat.as_slice()))
                }

                #[test]
                pub fn exclusion() {
                    let exclusion_pats = vec![
                        Path::new("a/b/").to_owned(),
                        Path::new("a/c").to_owned(),
                        Path::new("b").to_owned(),
                        Path::new("c").to_owned(),
                        Path::new("c/d").to_owned(),
                    ];

                    let matches_prefix_but_not_all_of_any_exclusion: SplitPath =
                        smallvec!["a".into()];
                    assert!(not_excluded(
                        &matches_prefix_but_not_all_of_any_exclusion,
                        &exclusion_pats
                    ));
                    let matches_exactly_one: SplitPath = smallvec!["a".into(), "b".into()];
                    assert!(!not_excluded(&matches_exactly_one, &exclusion_pats));
                    let matches_deeper_one: SplitPath =
                        smallvec!["a".into(), "c".into(), "innerfolder".into()];
                    assert!(!not_excluded(&matches_deeper_one, &exclusion_pats));
                    let matches_two: SplitPath =
                        smallvec!["c".into(), "d".into(), "deepfolder".into()];
                    assert!(!not_excluded(&matches_two, &exclusion_pats));
                }
            }

            /// Convenience functions for displaying local+remote as part of a more root cause
            /// table display but with added [local:] => [remote:] annotation.
            pub mod display_remote {
                use crate::table::{green, red, yellow};
                use std::fmt::Display;

                pub fn green_lr(
                    prefix: impl Display,
                    mid: impl Display,
                    local: impl Display,
                    remote: impl Display,
                ) {
                    green(prefix, format!("{mid}[local:{local}] => [remote:{remote}]"))
                }

                pub fn yellow_lr(
                    prefix: impl Display,
                    mid: impl Display,
                    local: impl Display,
                    remote: impl Display,
                ) {
                    yellow(prefix, format!("{mid}[local:{local}] => [remote:{remote}]"))
                }

                pub fn red_lr(
                    prefix: impl Display,
                    mid: impl Display,
                    local: impl Display,
                    remote: impl Display,
                ) {
                    red(prefix, format!("{mid}[local:{local}] => [remote:{remote}]"))
                }
            }

            /// The local path as a sequence of OsString, relative to the pushed directory, converted to a full unicode string for upload, or a full *OsString* path.
            ///
            /// This is all normalised and such as well.
            pub fn relative_remote_path_as_unicode(
                relative_to_local_dir_path: &SplitPath,
            ) -> Result<String, LocalPathUnicodeError> {
                relative_to_local_dir_path
                    .join(OsStr::new("/"))
                    .into_string()
                    .map(Cow::Owned)
                    .map(normalise_remote_neocities_path)
                    .map(Cow::into_owned)
                    .map_err(LocalPathUnicodeError)
            }

            /// Error in translating a local path to  
            #[derive(Debug)]
            pub struct LocalPathUnicodeError(pub OsString);

            impl Display for LocalPathUnicodeError {
                fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
                    write!(
                        f,
                        "Local path \"{}\" was not valid unicode (can't be pushed to remote)",
                        Path::new(&self.0).display()
                    )
                }
            }

            impl std::error::Error for LocalPathUnicodeError {}

            #[derive(thiserror::Error, Debug)]
            /// Error when loading paths and files from disk for upload.
            pub enum FileError {
                #[error(transparent)]
                LocalPathError(#[from] LocalPathUnicodeError),
                #[error(transparent)]
                IoError(#[from] std::io::Error),
            }
        }

        impl Cmd for Push {
            fn run(self) -> Result<(), anyhow::Error> {
                let Self {
                    auth,
                    dry_run,
                    prune,
                    exclude_files,
                    local_directory_to_push,
                    ignore_network_errors,
                    ignore_file_errors,
                    no_ignore_bad_hashes,
                    maximum_upload_concurrency,
                } = self;

                let client = Arc::new(auth.into_neocities_client());

                let runtime = make_runtime();

                let existing_files_list = runtime.block_on(client.list("")).or_else(|err| {
                    warn!("Error loading list of existing files, directories, and hashes from neocities.");
                    warn!("{err}");
                    if ignore_network_errors {
                        warn!("Continuing anyway without attempts at deduplication - pruning also will not work.");
                        Ok(vec![])
                    } else {
                        Err(anyhow!("Bailing out"))
                    }
                })?;
                // Build a hashmap of remote files - relative to remote root - for pruning.
                //
                // If the remote hash is None, then the remote was a directory.
                let mut remote_file_metadata = HashMap::new();
                push_cmd::build_existence_map(
                    &mut remote_file_metadata,
                    existing_files_list,
                    !no_ignore_bad_hashes,
                )?;

                // For async spawned task.
                let remote_file_metadata = Arc::new(remote_file_metadata);

                let client_preduplicated = client.clone();

                runtime.block_on(async move {
                    let (directory_traversal_task, directory_entry_stream) =
                        recursive_directory_traverser(local_directory_to_push, ignore_file_errors)
                            .await;

                    let (mut file_uploading_results, file_uploading_task_spawner) = restricted_parallel_streaming_async(
                        (remote_file_metadata.clone(), client.clone()), 
                        directory_entry_stream, 
                        move |v| match v {
                        Ok((mut relative_to_local_dir_directory_path, directory_entry, symlink_result_type)) => {
                                // Emission stream does not include the name of the actual entry
                                // inside the sent directory path. So we extend it here :) nya
                                let relative_to_local_dir_path = {
                                    relative_to_local_dir_directory_path.push(directory_entry.file_name());
                                    relative_to_local_dir_directory_path
                                };
                                if !not_excluded(&relative_to_local_dir_path, &exclude_files) {
                                    yellow_lr("NOT UPLOADING (matched exclusion filters)", "",  directory_entry.path().display(), Path::new(&relative_to_local_dir_path.as_slice().join(OsStr::new("/"))).display());
                                    return Err(ControlFlow::Continue(()));
                                };                                
                                let full_remote_path = wrap_err_as_control_flow(
                                    relative_remote_path_as_unicode(&relative_to_local_dir_path)
                                        .map_err(|e| {warn!("{e}"); FileError::LocalPathError(e)}), 
                                    ignore_file_errors
                                )?;
                                let full_local_path = directory_entry.path();

                                Ok((relative_to_local_dir_path, directory_entry, symlink_result_type, full_remote_path, full_local_path))
                        },
                        Err(file_err) => wrap_err_as_control_flow(Err(file_err.into()).map_err(|e| {warn!("File error"); warn!("{e}"); e}), ignore_file_errors),
                    }, move |(remote_file_metadata, client), (_relative_to_local_dir_path, _directory_entry, symlink_result_type, full_remote_path, full_local_path)| async move {
                            let mut hasher = sha1::Sha1::new();
                            match symlink_result_type {
                                SymlinkResolvedType::File => {
                                    let file_data = match tokio::fs::read(&full_local_path).await {
                                        Ok(d) => Ok(d),
                                        Err(file_err) => {
                                            warn!("Couldn't read file at local path {}", full_local_path.display());
                                            // Error logging in separate
                                            // process.
                                            Err(IoOrReqwestError::from(file_err))
                                        },
                                    }?;
                                    let local_hash = LocalSha1Hash({
                                        hasher.update(&file_data);
                                        hasher.finalize().into()
                                    });
                                    let do_upload = if let Some(remote_info) = remote_file_metadata.get(&full_remote_path) {
                                        remote_info.1.mark_exists();
                                        if remote_info.0.map_or(false, |v| local_hash == v) {
                                            yellow_lr("EXISTING REMOTE FILE (matching hashes)", "", full_local_path.display(), &full_remote_path);
                                            yellow_lr("NOT UPLOADING", "", full_local_path.display(), &full_remote_path);
                                            false
                                        } else {
                                            green_lr("EXISTING REMOTE FILE (hashes do not match)", "", full_local_path.display(), &full_remote_path);
                                            green_lr("UPLOADING (if not dry run)", "", full_local_path.display(), &full_remote_path);
                                            true
                                        }
                                    } else {
                                        green_lr("NEW FILE", "", full_local_path.display(), &full_remote_path);
                                        green_lr("UPLOADING (if not dry run)", "", full_local_path.display(), &full_remote_path);
                                        true
                                    };
                                    if do_upload {
                                        if dry_run {
                                            display_response_result_and_refine(Ok(
                                                    "dry run, no actual uploading happened".to_string()
                                                ))
                                                .map_err(IoOrReqwestError::from)
                                        } else {
                                            let result = client.upload(full_remote_path, file_data).await;
                                            display_response_result_and_refine(result).map_err(Into::into)
                                        }
                                    } else {
                                        Ok(())
                                    }
                                },
                                SymlinkResolvedType::Directory => {
                                    if let Some(remote_info) = remote_file_metadata.get(&full_remote_path) {
                                        remote_info.1.mark_exists();
                                        yellow_lr("EXISTING DIRECTORY", "", full_local_path.display(), full_remote_path);
                                        Ok(())
                                    } else {
                                        green_lr("NEW DIRECTORY", "", full_local_path.display(), full_remote_path);
                                        Ok(())
                                    }
                                },
                            }
                    }, maximum_upload_concurrency).await;

                    // Task that handles the results from the file uploading tasks spawned.
                    let error_handling_task: JoinHandle<Result<(), anyhow::Error>> = tokio::spawn(async move {
                        while let Some(next_task) = file_uploading_results.recv().await {
                            // In case of join errors, we always fail on those.
                            match next_task.await? {
                                Ok(_) => {},
                                Err(IoOrReqwestError::Reqwest(network_error)) => {
                                    warn!("Request error");
                                    warn!("{network_error}");
                                    if ignore_network_errors {
                                        warn!("Ignoring network error. Result may be broken in some way.");
                                        continue;
                                } else {
                                    error!("Terminating!");
                                    return Err(network_error.into())
                                }},
                                Err(IoOrReqwestError::Io(file_err)) => {
                                    warn!("File Error");
                                    warn!("{file_err}");
                                    if ignore_file_errors {
                                        warn!("Ignoring file error");
                                            continue;
                                    } else {
                                        error!("Terminating!");
                                        return Err(file_err.into());
                                    }
                                }
                            }
                        };

                        Ok(())
                    });
                    directory_traversal_task.await??;
                    file_uploading_task_spawner.await??;

                    // This waits for all the previous tasks before being finished.
                    error_handling_task.await??;

                    let client = client_preduplicated;

                    // Since all the existing files/dirs are now marked as such, we can prune the
                    // non-existent ones.
                    if prune {
                        println!("{}", green!("STARTING PRUNE OF REMOTE FILES NOT PRESENT IN LOCAL SELECTION"));
                        if dry_run {
                            println!("{}", yellow!("(dry run, no actual deletion)"));
                        } 

                        // TODO: when the never type (!) is stabilised, replace with that.
                        let (mut prune_tasks, prune_task_spawner) = restricted_parallel_async(
                            client, 
                            Arc::try_unwrap(remote_file_metadata).expect("All other references should be gone now since we waited for all the previous tasks.").into_iter(), 
                        |(path, (_hash, existence_tag))| -> Result<String, ControlFlow<ImpossibleErr>> {
                            if !existence_tag.exists() {
                                // Doesnt exist locally so we must! prune it nya
                                Ok(path)
                            } else {
                                Err(ControlFlow::Continue(()))
                            }
                        }, move |client,  remote_path_to_delete| async move {
                            red("DELETING REMOTE", remote_path_to_delete.as_str());
                            let result = if !dry_run {
                                client.delete(core::slice::from_ref(&remote_path_to_delete)).await
                            } else {
                                Ok("(dry run, not actually deleting)".to_owned())
                            };

                            display_response_result_and_refine(result).map_err(|e| {
                                warn!("Network error deleting remote {remote_path_to_delete}");
                                warn!("{e}");
                                e
                            })
                        }, maximum_upload_concurrency).await;
                        
                        // error is logged in the actual source of the error. 
                        let pruning_err_handling_task: JoinHandle<Result<(), anyhow::Error>> = tokio::spawn(async move {
                            while let Some(next_err_task) = prune_tasks.recv().await {
                                // Join errors always fail.
                                match next_err_task.await? {
                                    Ok(_) => {},
                                    Err(e) => if ignore_network_errors {
                                        warn!("Skipping");
                                        continue;
                                    } else {
                                        error!("Terminating!");
                                        Err(e)?;
                                        break;
                                    }
                                }
                            };
                            Ok(())
                        });
                        
                        prune_task_spawner.await??;
                        pruning_err_handling_task.await??; 
                    }
                    
                    Ok(())
                })
            }
        }

        #[derive(Bpaf, Clone)]
        #[bpaf(command, version)]
        /// Print license information about this program
        pub struct License {
            #[bpaf(short('f'), long("full"))]
            /// Whether to print the full GPLv3 or just the brief summary about redistribution.
            full: bool,
        }

        impl Cmd for License {
            fn run(self) -> Result<(), anyhow::Error> {
                const SMALL_LICENSE: &str = r#"rustcities Copyright (C) 2022  sapient_cogbag <sapient_cogbag at protonmail dot com> 
This program comes with ABSOLUTELY NO WARRANTY.
This is free and open-source software, and you are welcome to redistribute it under certain conditions.
Use 'rustcities license --full' for details."#;

                const FULL_LICENSE: &str = include_str!("../LICENSE");

                if self.full {
                    // We want newline on the end to avoid having prompt glued on
                    println!("{}", FULL_LICENSE);
                } else {
                    // We want newline on the end to avoid having prompt glued on
                    println!("{}", SMALL_LICENSE);
                }

                Ok(())
            }
        }
    }
}

use cli::Cmd;

fn main() -> Result<(), anyhow::Error> {
    simplelog::TermLogger::init(
        LevelFilter::Info,
        simplelog::Config::default(),
        simplelog::TerminalMode::Stderr,
        simplelog::ColorChoice::Auto,
    )?;

    let parser = cli::full_command();
    parser.run().run()
}

// rustcities
// Copyright (C) 2022  sapient_cogbag <sapient_cogbag at protonmail dot com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
