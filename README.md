# rustcities
Simple CLI for the neocities API, using [this library](https://crates.io/crates/neocities), and doing "smart" upload deduplication just like [the original ruby neocities cli](https://github.com/neocities/neocities-ruby), by checking file hashes.

## Why yet another CLI? 
There are lots of neocities CLIs around - there's one in Ruby, one in Go, one in bash. Unfortunately, they all have very severe packaging issues - the Go package on the AUR simply doesn't build, the Ruby one may or may not be broken (and it installs out of date packages too), the bash one is written in bash and requires Git or it ends up janky and the interface is kind of unclear.

Rust is a very reliable language for a number of reasons. As such, I'm rewriting things in Rust for the purposes of easing packaging, and providing structured CLI parsing. This is designed for ease of use with my own website.
