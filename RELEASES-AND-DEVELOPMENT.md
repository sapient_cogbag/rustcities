Releases are defined by signed git tags of the name `v<MAJOR>.<MINOR>.<BUGFIX>`, on the `main` branch.

The first major release was `v1.0.0`. Now this release has happened, future development will be on the `dev` branch, and 
the `main` branch should build fine.


